package com.peoplewalking.scanner.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.peoplewalking.scanner.models.Franja;
import com.peoplewalking.scanner.models.Ticket;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 02-26-16.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();
    private static final String DATABASE_NAME = "ticket.db";
    private static final int DATABASE_VERSION = 5; //change when database objects have been changed
    private Dao<Franja, Integer> franjaDao = null;
    private Dao<Ticket, Integer> ticketDao = null;
    private Context mContext;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    /**
     * This is called when the database is first created. Usually you should call createTable statements here to create
     * the tables that will store your data.
     */
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Franja.class);
            TableUtils.createTable(connectionSource, Ticket.class);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Franja.class, true);
            TableUtils.dropTable(connectionSource, Ticket.class, true);
            onCreate(db, connectionSource);
        } catch (java.sql.SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the Database Access Object (DAO) for our SimpleData class. It will create it or just give the cached
     * value.
     */
    public Dao<Franja, Integer> getSimpleDataDao() throws java.sql.SQLException {
        if (franjaDao == null) {
            franjaDao = getDao(Franja.class);
        }
        return franjaDao;
    }

    public Dao<Ticket, Integer> getTicketDao() throws java.sql.SQLException {
        if (ticketDao == null) {
            ticketDao = getDao(Ticket.class);
        }
        return ticketDao;
    }

    public Franja getFranja(String eventCode) {
        try {
            List<Franja> list = getSimpleDataDao().queryForEq("evento", eventCode);
            return list.get(0);
        } catch (Exception ex) {
            return null;
        }
    }

    public Franja getFranjaById(String id) {
        try {
            List<Franja> list = getSimpleDataDao().queryForEq("id", id);
            return list.get(0);
        } catch (Exception ex) {
            return null;
        }
    }

    public Franja getFirstFranja() {
        try {
            List<Franja> list = getSimpleDataDao().queryForAll();
            return list.get(0);
        } catch (Exception ex) {
            return null;
        }
    }


    public Ticket getTicket(String value) {
        try {
            List<Ticket> list = getTicketDao().queryForEq("value", value);
            return list.get(0);
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean updateFranjas(ArrayList<Franja> franjas) {
        try {
            for (Franja f : franjas) {
                getSimpleDataDao().createOrUpdate(f);
            }
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean setFranja(Franja franja) {
        try {
            Dao<Ticket, Integer> tDao = getTicketDao();
            List<Ticket> tickets = tDao.queryForAll();
            tDao.delete(tickets);
            Dao<Franja, Integer> dao = getSimpleDataDao();
            List<Franja> franjas = dao.queryForAll();
            dao.delete(franjas);
            getSimpleDataDao().createOrUpdate(franja);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean addTicket(Ticket ticket) {
        try {
            getTicketDao().createOrUpdate(ticket);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean deleteDatabase() {
        close();
        try {
            mContext.deleteDatabase(DATABASE_NAME);
            Log.i(TAG, "deleteDatabase: Database deleted");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public void close() {
        super.close();
        franjaDao = null;
        ticketDao = null;
    }
}
