package com.peoplewalking.scanner.configs;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

/**
 * Created by pwk04 on 11-08-16.
 */
public class App extends Application {

    private boolean franja;
    private SharedPreferences sharedPreferences;
    private static App instance;
    private Context context;
    private static String userName = "Openbravo";
    private static String password = "openbravo";
    private static String serviceURL = "/openbravo/org.openbravo.service.json.jsonrest/";

    public static String getUserName() {
        return userName;
    }

    public static void setUserName(String userName) {
        App.userName = userName;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        App.password = password;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static App getInstance(){
        return instance;
    }

    public static String getBasicAuthentication() {
        final String credentials = userName + ":" + password;
        String string = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        return string;
    }

    public boolean isFranja() {
        return franja;
    }

    public void setFranja(boolean franja) {
        this.franja = franja;
    }

    private SharedPreferences getSharedPreferences(){
        if(sharedPreferences == null)
            sharedPreferences = getSharedPreferences("TICKETS_APP", Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    public void editSharedPreference(String preferenceName, String val){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(preferenceName, val);
        editor.commit();
    }

    /**
     * Edita un parámetro de configuración (SharedPreference)
     * @param preferenceName
     * @param val valor asignado
     * @author Carlos Delgadillo
     * @fecha 06/05/2015
     */
    public void editSharedPreference(String preferenceName, int val){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(preferenceName, val);
        editor.commit();
    }

    public void setServer(String server){
        editSharedPreference("server", server);
    }

    public boolean isServerSet(){
        if(getServer() == null)
            return false;
        return true;
    }

    public String getServer(){
        String server = getSharedPreferences().getString("server", null);
        return server;
    }


}
