package com.peoplewalking.scanner.configs;

import android.util.Base64;

/**
 * Created by root on 02-25-16.
 */
public class AppConfig {
    public static final String serverName = "http://192.168.10.60:8080";
    public static final String serviceURL = "/openbravo/org.openbravo.service.json.jsonrest/";
    public static final String userName = "Openbravo";
    public static final String password = "openbravo";

    public static String getBasicAuthentication() {
        final String credentials = userName + ":" + password;
        String string = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        return string;
    }
}
