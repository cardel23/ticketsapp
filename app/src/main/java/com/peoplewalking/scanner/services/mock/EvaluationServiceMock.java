package com.peoplewalking.scanner.services.mock;

import android.os.Handler;

import com.peoplewalking.scanner.models.Boleto;
import com.peoplewalking.scanner.models.Event;
import com.peoplewalking.scanner.models.Franja;
import com.peoplewalking.scanner.models.OpenbravoResponse;
import com.peoplewalking.scanner.models.OrderLine;
import com.peoplewalking.scanner.models.Product;
import com.peoplewalking.scanner.services.IEvaluationService;
import com.peoplewalking.scanner.services.IResultCallback;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by root on 02-23-16.
 */
public class EvaluationServiceMock implements IEvaluationService {
    /*@Override
    public void CheckCode(String code, final IResultCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Random random = new Random();
                int val = random.nextInt();
                if (val % 2 == 0) {
                    callback.onError();
                } else {
                    callback.onSuccess();
                }
            }
        }, 2000);
    }*/

    @Override
    public void getEvent(IResultCallback callback) {

    }

    @Override
    public void getProduct(String id, IResultCallback callback) {

    }

    @Override
    public void getFranja(IResultCallback callback) {

    }


    @Override
    public void getProductCategory(IResultCallback callback) {

    }

    @Override
    public void getPricingProductPrice(IResultCallback callback) {

    }

    @Override
    public void getOrder(IResultCallback callback) {

    }

    @Override
    public void getFranjaByEventId(String code, IResultCallback<OpenbravoResponse<ArrayList<Franja>>> callback) {

    }

    @Override
    public void getBoletoByValue(String code, IResultCallback<Boleto> callback) {

    }

    @Override
    public void getOrderLine(String code, IResultCallback<OrderLine> callback) {

    }

    @Override
    public void getProductById(String id, IResultCallback<Product> callback) {

    }

    @Override
    public void getEventById(String id, IResultCallback<Event> callback) {

    }

    @Override
    public void getFranjaById(String franjaId, IResultCallback<Franja> callback) {

    }

    @Override
    public void getProductsByEvent(String eventId, IResultCallback<ArrayList<Product>> callback) {

    }

    @Override
    public void getOrderLinesByProductId(String productId, IResultCallback<ArrayList<OrderLine>> callback) {

    }

    @Override
    public void getBoletosByOrderLine(String orderLineId, IResultCallback<ArrayList<Boleto>> callback) {

    }

    @Override
    public void getProductsByFranja(String franjaId, IResultCallback<ArrayList<Product>> callback) {

    }

}
