package com.peoplewalking.scanner.services;

/**
 * Created by root on 02-23-16.
 */
public interface IResultCallback<T> {
    void onSuccess(T response);
    void onError(String message);
}
