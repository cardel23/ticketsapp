package com.peoplewalking.scanner.services;

import android.util.Log;

import com.peoplewalking.scanner.configs.App;
import com.peoplewalking.scanner.configs.AppConfig;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by root on 02-25-16.
 */
public class HttpClient {
    private static App app;
    public static Retrofit getRetrofit() {

        app = App.getInstance();
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        Log.i("url:", request.url().toString());
                        Request newRequest = request.newBuilder()
                                .header("Authorization", app.getBasicAuthentication())
                                .header("Accept", "application/json")
                                .build();
                        okhttp3.Response originalResponse = chain.proceed(newRequest);
                        Log.i("Response:", String.valueOf(originalResponse.isSuccessful()));
                        return originalResponse;
                    }
                })

                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .readTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.serverName + AppConfig.serviceURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit;
    }
}
