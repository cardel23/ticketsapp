package com.peoplewalking.scanner.services;

import com.peoplewalking.scanner.models.Boleto;
import com.peoplewalking.scanner.models.Event;
import com.peoplewalking.scanner.models.Franja;
import com.peoplewalking.scanner.models.OpenbravoResponse;
import com.peoplewalking.scanner.models.OrderLine;
import com.peoplewalking.scanner.models.Product;

import java.util.ArrayList;

/**
 * Created by root on 02-23-16.
 */
public interface IEvaluationService {
    void getEvent(final IResultCallback callback);

    void getFranja(final IResultCallback<OpenbravoResponse<ArrayList<Franja>>> callback);

    void getProduct(String id, final IResultCallback callback);

    void getProductCategory(final IResultCallback callback);

    void getPricingProductPrice(final IResultCallback callback);

    void getOrder(final IResultCallback callback);

    void getFranjaByEventId(String code, final IResultCallback<OpenbravoResponse<ArrayList<Franja>>> callback);

    void getBoletoByValue(String code, final IResultCallback<Boleto> callback);

    void getOrderLine(String code, final IResultCallback<OrderLine> callback);

    void getProductById(String id, final IResultCallback<Product> callback);

    void getEventById(String id, final IResultCallback<Event> callback);

    void getFranjaById(String franjaId, final IResultCallback<Franja> callback);

    void getProductsByEvent(String eventId, IResultCallback<ArrayList<Product>> callback);

    void getOrderLinesByProductId(String productId, IResultCallback<ArrayList<OrderLine>> callback);

    void getBoletosByOrderLine(String orderLineId, IResultCallback<ArrayList<Boleto>> callback);

    void getProductsByFranja(String franjaId, IResultCallback<ArrayList<Product>> callback);
}
