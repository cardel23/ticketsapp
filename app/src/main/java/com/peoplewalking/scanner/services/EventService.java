package com.peoplewalking.scanner.services;

import com.peoplewalking.scanner.models.Boleto;
import com.peoplewalking.scanner.models.Event;
import com.peoplewalking.scanner.models.Franja;
import com.peoplewalking.scanner.models.OpenbravoResponse;
import com.peoplewalking.scanner.models.OrderLine;
import com.peoplewalking.scanner.models.Product;
import com.peoplewalking.scanner.models.ResponseWrapper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by root on 02-23-16.
 */
public class EventService implements IEvaluationService {
    private static final String TAG = EventService.class.getSimpleName();
    private RetrofitEventService service;

    public EventService() {
        Retrofit retrofit = HttpClient.getRetrofit();
        service = retrofit.create(RetrofitEventService.class);
    }

    @Override
    public void getEvent(final IResultCallback callback) {

    }

    @Override
    public void getProduct(String id, IResultCallback callback) {

    }

    @Override
    public void getFranja(final IResultCallback callback) {
        service.getFranja().enqueue(new Callback<ResponseWrapper<OpenbravoResponse<ArrayList<Franja>>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<OpenbravoResponse<ArrayList<Franja>>>> call, Response<ResponseWrapper<OpenbravoResponse<ArrayList<Franja>>>> response) {
                if (response.isSuccess()) {
                    callback.onSuccess(response.body().getResponse());
                } else {
                    callback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<OpenbravoResponse<ArrayList<Franja>>>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }


    @Override
    public void getProductCategory(final IResultCallback callback) {

    }

    @Override
    public void getPricingProductPrice(final IResultCallback callback) {
        service.getPricingProductPrice().enqueue(new Callback<ResponseWrapper<OpenbravoResponse<Object>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<OpenbravoResponse<Object>>> call, Response<ResponseWrapper<OpenbravoResponse<Object>>> response) {
                if (response.isSuccess()) {
                    callback.onSuccess(response.body().getResponse());
                } else {
                    new Exception(response.message()).printStackTrace();
                    callback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<OpenbravoResponse<Object>>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getOrder(final IResultCallback callback) {

    }

    @Override
    public void getFranjaByEventId(String code, final IResultCallback<OpenbravoResponse<ArrayList<Franja>>> callback) {
        String query = String.format("evento='%1s'", code);
        service.getFranjas(query).enqueue(new Callback<ResponseWrapper<OpenbravoResponse<ArrayList<Franja>>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<OpenbravoResponse<ArrayList<Franja>>>> call, Response<ResponseWrapper<OpenbravoResponse<ArrayList<Franja>>>> response) {
                if (response.isSuccess()) {
                    callback.onSuccess(response.body().getResponse());
                } else {
                    callback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<OpenbravoResponse<ArrayList<Franja>>>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getBoletoByValue(String code, final IResultCallback<Boleto> callback) {
        String query = String.format("value='%1s'", code);
        service.getBoletos(query).enqueue(new Callback<ResponseWrapper<OpenbravoResponse<ArrayList<Boleto>>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<OpenbravoResponse<ArrayList<Boleto>>>> call, Response<ResponseWrapper<OpenbravoResponse<ArrayList<Boleto>>>> response) {
                if (response.isSuccess()) {
                    ArrayList<Boleto> boletos = response.body().getResponse().getData();

                    if (boletos.size() > 0) {
                        Boleto boleto = boletos.get(0);
                        callback.onSuccess(boleto);
                    } else {
                        //TODO: Internationalize text
                        callback.onError("Boleto no encontrado.");
                    }
                } else {
                    callback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<OpenbravoResponse<ArrayList<Boleto>>>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getOrderLine(String code, final IResultCallback<OrderLine> callback) {
        service.getOrderLine(code).enqueue(new Callback<OrderLine>() {
            @Override
            public void onResponse(Call<OrderLine> call, Response<OrderLine> response) {
                if (response.isSuccess()) {
                    callback.onSuccess(response.body());
                } else {
                    callback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<OrderLine> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getProductById(String id, final IResultCallback<Product> callback) {
        service.getProductById(id).enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                if (response.isSuccess()) {
                    callback.onSuccess(response.body());
                } else {
                    callback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getEventById(String id, final IResultCallback<Event> callback) {
        service.getEventById(id).enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getFranjaById(String franjaId, final IResultCallback<Franja> callback) {
        service.getFranjaById(franjaId).enqueue(new Callback<Franja>() {
            @Override
            public void onResponse(Call<Franja> call, Response<Franja> response) {
                if (response.isSuccess()) {
                    callback.onSuccess(response.body());
                } else {
                    if (response.code() == 404) {
                        callback.onError("No se encontró una franja con ese código.");
                    } else {
                        callback.onError(response.message());
                    }

                }
            }

            @Override
            public void onFailure(Call<Franja> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getProductsByEvent(String eventId, final IResultCallback<ArrayList<Product>> callback) {
        String query = String.format("tpEvento='%1s'", eventId);
        service.getProducts(query).enqueue(new Callback<ResponseWrapper<OpenbravoResponse<ArrayList<Product>>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<OpenbravoResponse<ArrayList<Product>>>> call, Response<ResponseWrapper<OpenbravoResponse<ArrayList<Product>>>> response) {
                if (response.isSuccess()) {
                    callback.onSuccess(response.body().getResponse().getData());
                } else {
                    callback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<OpenbravoResponse<ArrayList<Product>>>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getOrderLinesByProductId(String productId, final IResultCallback<ArrayList<OrderLine>> callback) {
        String query = String.format("product='%1s'", productId);
        service.getOrderLines(query).enqueue(new Callback<ResponseWrapper<OpenbravoResponse<ArrayList<OrderLine>>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<OpenbravoResponse<ArrayList<OrderLine>>>> call, Response<ResponseWrapper<OpenbravoResponse<ArrayList<OrderLine>>>> response) {
                if (response.isSuccess()) {
                    callback.onSuccess(response.body().getResponse().getData());
                } else {
                    callback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<OpenbravoResponse<ArrayList<OrderLine>>>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getBoletosByOrderLine(String orderLineId, final IResultCallback<ArrayList<Boleto>> callback) {
        String query = String.format("orderline='%1s'", orderLineId);
        service.getBoletos(query).enqueue(new Callback<ResponseWrapper<OpenbravoResponse<ArrayList<Boleto>>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<OpenbravoResponse<ArrayList<Boleto>>>> call, Response<ResponseWrapper<OpenbravoResponse<ArrayList<Boleto>>>> response) {
                if (response.isSuccess()) {
                    callback.onSuccess(response.body().getResponse().getData());
                } else {
                    callback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<OpenbravoResponse<ArrayList<Boleto>>>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getProductsByFranja(String franjaId, final IResultCallback<ArrayList<Product>> callback) {
        String query = String.format("tpFranja='%1s'", franjaId);
        service.getProducts(query).enqueue(new Callback<ResponseWrapper<OpenbravoResponse<ArrayList<Product>>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<OpenbravoResponse<ArrayList<Product>>>> call, Response<ResponseWrapper<OpenbravoResponse<ArrayList<Product>>>> response) {
                if (response.isSuccess()) {
                    callback.onSuccess(response.body().getResponse().getData());
                } else {
                    callback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<OpenbravoResponse<ArrayList<Product>>>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    public interface RetrofitEventService {

        @GET("TP_Evento")
        Call<ResponseWrapper<OpenbravoResponse<ArrayList<Event>>>> getEvent();

        @GET("TP_Evento/{EventId}")
        Call<Event> getEventById(@Path("EventId") String id);

        @GET("TP_Franja")
        Call<ResponseWrapper<OpenbravoResponse<ArrayList<Franja>>>> getFranja();

        @GET("TP_Franja/{franjaId}")
        Call<Franja> getFranjaById(@Path("franjaId") String id);

        @GET("Product")
        Call<ResponseWrapper<OpenbravoResponse<Object>>> getProduct();

        @GET("Product/{productId}")
        Call<Product> getProductById(@Path("productId") String id);

        @GET("Product")
        Call<ResponseWrapper<OpenbravoResponse<ArrayList<Product>>>> getProducts(@Query("_where") String query);

        @GET("ProductCategory")
        Call<ResponseWrapper<OpenbravoResponse<Object>>> getProductCategory();

        @GET("PricingProductPrice")
        Call<ResponseWrapper<OpenbravoResponse<Object>>> getPricingProductPrice();

        @GET("Order")
        Call<ResponseWrapper<OpenbravoResponse<Object>>> getOrder();

        @GET("TP_Franja")
        Call<ResponseWrapper<OpenbravoResponse<ArrayList<Franja>>>> getFranjas(@Query("_where") String query);

        @GET("TP_Boleto")
        Call<ResponseWrapper<OpenbravoResponse<ArrayList<Boleto>>>> getBoletos(@Query("_where") String query);

        @GET("OrderLine/{orderLineId}")
        Call<OrderLine> getOrderLine(@Path("orderLineId") String id);

        @GET("OrderLine")
        Call<ResponseWrapper<OpenbravoResponse<ArrayList<OrderLine>>>> getOrderLines(@Query("_where") String query);
    }

}
