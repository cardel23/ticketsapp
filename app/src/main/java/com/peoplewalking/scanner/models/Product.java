package com.peoplewalking.scanner.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 02-25-16.
 */
public class Product {
    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }


    public String getFrangaId() {
        return frangaId;
    }

    public void setFrangaId(String frangaId) {
        this.frangaId = frangaId;
    }

    @SerializedName("tpFranja")
    private String frangaId;
    @SerializedName("tpEvento")
    private String eventId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @SerializedName("id")
    private String id;
}
