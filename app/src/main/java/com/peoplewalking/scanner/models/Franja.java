package com.peoplewalking.scanner.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by root on 02-25-16.
 */
@DatabaseTable(tableName = "TP_Franja")
public class Franja {

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String evento$_identifier;

    @DatabaseField
    private String fechaInicio;

    @DatabaseField
    private String fechaFin;

    @DatabaseField
    private Boolean active;

    @DatabaseField
    private String evento;


    @DatabaseField
    private boolean ismultientry;

    private String name;

    private String _identifier;

    public String getEvento() {
        return evento;
    }

    public void setEvento(String evento) {
        this.evento = evento;
    }

    public boolean ismultientry() {
        return ismultientry;
    }

    public void setIsmultientry(boolean ismultientry) {
        this.ismultientry = ismultientry;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEvento$_identifier() {
        return evento$_identifier;
    }

    public void setEvento$_identifier(String evento$_identifier) {
        this.evento$_identifier = evento$_identifier;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



/*
                "_identifier": "Única función",
                "_entityName": "TP_Franja",
                "$ref": "TP_Franja/51D07B6DE1AC42A7953D44DC5F83ED46",
                "id": "51D07B6DE1AC42A7953D44DC5F83ED46",
                "client": "9C1CCABA1D334DDAA028832935951E16",
                "client$_identifier": "Ticketwalking",
                "organization": "383A81B6833E4A18AEAB1EADFA166C93",
                "organization$_identifier": "TW Spain",
                "active": true,
                "creationDate": "2016-02-25T13:55:37+00:00",
                "createdBy": "100",
                "createdBy$_identifier": "Openbravo",
                "updated": "2016-02-25T13:55:37+00:00",
                "updatedBy": "100",
                "updatedBy$_identifier": "Openbravo",
                "fechaInicio": "2016-02-26T01:00:00+00:00",
                "fechaFin": "2016-02-26T04:00:00+00:00",
                "isfreesale": true,
                "cantidad": null,
                "evento": "E65B009ED66E43D182F1CF742F4B092A",
                "evento$_identifier": "Joss Stone en Nicaragua",
                "name": "Única función",
                "ismultientry": false,
                "recordTime": 1456426909187*/

}
