package com.peoplewalking.scanner.models;

/**
 * Created by root on 03-03-16.
 */
public class OrderLine {
    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    private String product;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

}
