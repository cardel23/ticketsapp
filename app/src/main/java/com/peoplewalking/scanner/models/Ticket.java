package com.peoplewalking.scanner.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by root on 03-04-16.
 */
@DatabaseTable(tableName = "Ticket")
public class Ticket {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Boolean getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(Boolean isUsed) {
        this.isUsed = isUsed;
    }

    @DatabaseField(id = true)
    String id;

    @DatabaseField()
    String value;

    @DatabaseField()
    String startDate;

    @DatabaseField()
    String endDate;

    @DatabaseField()
    Boolean isUsed;

}
