package com.peoplewalking.scanner.models;

/**
 * Created by root on 02-25-16.
 */
public class Event {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation$_identifier() {
        return location$_identifier;
    }

    public void setLocation$_identifier(String location$_identifier) {
        this.location$_identifier = location$_identifier;
    }

    private String name;
    private String location$_identifier;


    /*"_identifier": "Joss Stone en Nicaragua",
                "_entityName": "TP_Evento",
                "$ref": "TP_Evento/E65B009ED66E43D182F1CF742F4B092A",
                "id": "E65B009ED66E43D182F1CF742F4B092A",
                "client": "9C1CCABA1D334DDAA028832935951E16",
                "client$_identifier": "Ticketwalking",
                "organization": "383A81B6833E4A18AEAB1EADFA166C93",
                "organization$_identifier": "TW Spain",
                "active": true,
                "creationDate": "2016-02-25T13:53:40+00:00",
                "createdBy": "100",
                "createdBy$_identifier": "Openbravo",
                "updated": "2016-02-25T13:53:40+00:00",
                "updatedBy": "100",
                "updatedBy$_identifier": "Openbravo",
                "name": "Joss Stone en Nicaragua",
                "description": null,
                "location": "5D2785B326584BD1893BBB3082D42A86",
                "location$_identifier": "Teatro Nacional Rubén Darío, Barrio Rubén Darío - Avenida Bolívar -  - Managua -  - Nicaragua",
                "value": "josstone",
                "recordTime": 1456426427538
                */
}
