package com.peoplewalking.scanner.models;

import android.renderscript.RenderScript;

/**
 * Created by root on 02-25-16.
 */
public class ResponseWrapper<T>{
    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    private T response;
}
