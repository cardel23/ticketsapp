package com.peoplewalking.scanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.Button;
import android.widget.TextView;

import com.google.zxing.integration.Android.IntentIntegrator;
import com.google.zxing.integration.Android.IntentResult;
import com.peoplewalking.scanner.configs.App;
import com.peoplewalking.scanner.presenters.IResultPresenter;
import com.peoplewalking.scanner.presenters.ResultPresenter;
import com.peoplewalking.scanner.views.IResultView;

import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity implements IResultView {


    private static final String TAG = MainActivity.class.getSimpleName();

    private Button mBtnScan;
    private IResultPresenter mPresenter;
    private TextView mResultView;
    private TextView mResultTextView;
    private String mLastCode;
    private View mContentView;
    private TextView mCodeView;
    private Button mBtnValidate;

    private boolean readingFranjaCode;
    private Options option = null;
    private final static int SET_SERVER_CODE = 1;
    private final static int CONFIG_TIME_STRIP_CODE = 49374;
    private App app;

    private enum Options {
        OptionA, OptionB
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ignored) {
        }
        if(app == null)
            app = (App) getApplicationContext();

        setContentView(R.layout.activity_main);
        if(!app.isServerSet()){
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivityForResult(intent,SET_SERVER_CODE);
        }
        else {
            initView();
            mPresenter = new ResultPresenter(app, this);
        }
    }

    private void initView() {
        mBtnScan = (Button) findViewById(R.id.btn_scan);
        mResultView = (TextView) findViewById(R.id.result);
        mResultTextView = (TextView) findViewById(R.id.result_view);
        mContentView = findViewById(R.id.content_layout);
        mCodeView = (TextView) findViewById(R.id.et_code);
        mBtnValidate = (Button) findViewById(R.id.btn_validate);
        mBtnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPresenter.isFranjaSetted()) {
                    scanBarCode(MainActivity.this);
                } else {
                    showDialog();
                    option = Options.OptionA;
                }
            }
        });

        mBtnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLastCode = mCodeView.getText().toString();
                if (mLastCode.equals("")) {
                    showInfo("El código del boleto es requerido.");
                    return;
                }


                if (mPresenter.isFranjaSetted()) {
                    mPresenter.checkCode(mLastCode);
                } else {
                    showDialog();
                    option = Options.OptionB;
                }

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode){
            case SET_SERVER_CODE:
                initView();
                mPresenter = new ResultPresenter(getApplicationContext(), this);
                break;

            case CONFIG_TIME_STRIP_CODE:
                IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
                if (scanResult != null && scanResult.getContents() != null) {
                    if (app.isFranja()) {
                        mPresenter.setFranja(scanResult.getContents());
                    } else {
                        mLastCode = scanResult.getContents();
                        mCodeView.setText(mLastCode);
                        Log.i(TAG, mLastCode);
                        mPresenter.checkCode(mLastCode);
                    }
                }
                readingFranjaCode = false;
                app.setFranja(readingFranjaCode);
                break;
        }
    }

    private void scanBarCode(Activity activity) {
        IntentIntegrator scanIntegrator = new IntentIntegrator(activity);
        scanIntegrator.initiateScan();
    }

    @Override
    public void onSuccessCode() {
        finishLoading();
        mResultTextView.setVisibility(View.VISIBLE);
        mResultView.setText(getString(R.string.valid) + " " + mLastCode);
        mResultTextView.setText("\u2713");
        mContentView.setBackgroundColor(getResources().getColor(R.color.success));
    }

    @Override
    public void onErrorCode() {
        finishLoading();
        mResultTextView.setVisibility(View.VISIBLE);
        mResultView.setText(getString(R.string.invalid) + " " + mLastCode);
        mResultTextView.setText("X");
        mContentView.setBackgroundColor(getResources().getColor(R.color.error));
    }

    @Override
    public void onLoading() {
        findViewById(R.id.content_layout).setVisibility(View.INVISIBLE);
        findViewById(R.id.loading_layout).setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorMessage(String message) {
        finishLoading();
        Snackbar.make(mContentView, message, Snackbar.LENGTH_LONG).show();
        //mContentView.setBackgroundColor(getResources().getColor(R.color.white));
    }

    @Override
    public void showInfo(String message) {
        Snackbar.make(mContentView, message, Snackbar.LENGTH_LONG).show();
    }

    public void finishLoading() {
        findViewById(R.id.content_layout).setVisibility(View.VISIBLE);
        findViewById(R.id.loading_layout).setVisibility(View.INVISIBLE);
        mContentView.setBackgroundColor(getResources().getColor(R.color.white));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.clear_cache:
                onLoading();
                mPresenter.clearCache();
                finishLoading();
                return true;
            case R.id.set_franja:
                readFranjaCode();
                return app.isFranja();
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void readFranjaCode() {
        boolean isTest = false;
        if (isTest) {
            mPresenter.setFranja("C4BE96B359C24999B16DB73728B567B1");
        } else {
            readingFranjaCode = true;
            app.setFranja(readingFranjaCode);
            IntentIntegrator scanIntegrator = new IntentIntegrator(this);
            scanIntegrator.initiateScan();
        }


    }

    private void showDialog() {
        AlertDialog.Builder scanFranjaDialog = new AlertDialog.Builder(this);
        scanFranjaDialog.setTitle("Configurar Franja");
        scanFranjaDialog.setMessage("Esta operación requiere una franja. ¿Desea Configurarla?");
        scanFranjaDialog.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                readFranjaCode();
            }
        });
        scanFranjaDialog.setNegativeButton("No", null);
        scanFranjaDialog.setCancelable(true);
        scanFranjaDialog.show();
    }

    @Override
    public void scanCode() {
        if (option == Options.OptionA) {
            scanBarCode(this);
        } else if (option == Options.OptionB) {
            mLastCode = mCodeView.getText().toString();
            mPresenter.checkCode(mLastCode);
        }
        option = null;
    }
}
