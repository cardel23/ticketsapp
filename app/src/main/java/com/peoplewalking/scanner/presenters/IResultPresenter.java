package com.peoplewalking.scanner.presenters;

/**
 * Created by root on 02-23-16.
 */
public interface IResultPresenter {
    void checkCode(String code);

    void clearCache();

    void setFranja(String franjaCode);

    boolean isFranjaSetted();
}
