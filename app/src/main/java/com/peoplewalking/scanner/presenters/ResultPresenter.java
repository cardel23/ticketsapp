package com.peoplewalking.scanner.presenters;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.peoplewalking.scanner.db.DatabaseHelper;
import com.peoplewalking.scanner.models.Boleto;
import com.peoplewalking.scanner.models.Event;
import com.peoplewalking.scanner.models.Franja;
import com.peoplewalking.scanner.models.OpenbravoResponse;
import com.peoplewalking.scanner.models.OrderLine;
import com.peoplewalking.scanner.models.Product;
import com.peoplewalking.scanner.models.Ticket;
import com.peoplewalking.scanner.services.EventService;
import com.peoplewalking.scanner.services.IEvaluationService;
import com.peoplewalking.scanner.services.IResultCallback;
import com.peoplewalking.scanner.views.IResultView;

import java.io.Console;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Handler;

import retrofit2.http.Path;

/**
 * Created by root on 02-23-16.
 */
public class ResultPresenter implements IResultPresenter {
    private static final String TAG = ResultPresenter.class.getSimpleName();

    private IResultView mResultView;
    private IEvaluationService mService;
    private Context mContext;
    DatabaseHelper mhelper;

    public ResultPresenter(Context context, IResultView resultView) {
        mResultView = resultView;
        mService = new EventService();
        mContext = context;
        mhelper = new DatabaseHelper(mContext);
    }

    @Override
    public void checkCode(final String code) {
        mResultView.onLoading();
        //Check code locally. If it doesn't exist locally request codes.
        final Ticket ticket = mhelper.getTicket(code);

        if (ticket != null) {
            if (ticket.getIsUsed()) {
                mResultView.showErrorMessage("La ticket ya ha sido usada");
                mResultView.onErrorCode();
            } else {
                if (isTodayInBetween(ticket.getStartDate(), ticket.getEndDate())) {
                    mResultView.onSuccessCode();
                    Franja f = mhelper.getFirstFranja();
                    if (!f.ismultientry()) {
                        ticket.setIsUsed(true);
                        mhelper.addTicket(ticket);
                    }
                } else {
                    mResultView.showInfo("El código no es válido para hoy");
                    mResultView.onErrorCode();
                }
            }
        } else {
            //TODO: Refactor with chain of responsibility.
            mService.getBoletoByValue(code, new IResultCallback<Boleto>() {
                @Override
                public void onSuccess(Boleto response) {

                    final Ticket serverTicket = new Ticket();
                    serverTicket.setId(response.getId());
                    serverTicket.setValue(response.getValue());

                    mService.getOrderLine(response.getOrderline(), new IResultCallback<OrderLine>() {
                        @Override
                        public void onSuccess(OrderLine response) {
                            mService.getProductById(response.getProduct(), new IResultCallback<Product>() {

                                @Override
                                public void onSuccess(final Product response) {
                                    if (response.getEventId() == null) {
                                        mResultView.showErrorMessage("El codigo no tiene franjas asociadas");
                                        return;
                                    }

                                    //new thread to download in background tickets
                                    /*Thread t = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            getEventData(response.getEventId());
                                        }
                                    });
                                    t.start();*/

                                    Franja franja = mhelper.getFranjaById(response.getFrangaId());
                                    if (franja == null) {
                                        mResultView.showErrorMessage("El boleto no es válido para la franja configurada");
                                        mResultView.onErrorCode();
                                        return;
                                    }

                                    serverTicket.setEndDate(franja.getFechaFin());
                                    serverTicket.setStartDate(franja.getFechaInicio());

                                    if (franja != null && isTodayInBetween(franja.getFechaInicio(), franja.getFechaFin())) {
                                        mResultView.onSuccessCode();
                                        Ticket bdticket = mhelper.getTicket(serverTicket.getValue());
                                        if (bdticket != null) {
                                            bdticket.setIsUsed(franja.ismultientry() ? false : true);
                                            mhelper.addTicket(bdticket);
                                        } else {
                                            serverTicket.setIsUsed(franja.ismultientry() ? false : true);
                                            mhelper.addTicket(serverTicket);
                                        }


                                    } else {
                                        mResultView.onErrorCode();
                                    }
                                }

                                @Override
                                public void onError(String message) {
                                    mResultView.showErrorMessage(message);
                                }
                            });

                        }

                        @Override
                        public void onError(String message) {
                            mResultView.showErrorMessage(message);
                        }
                    });
                }

                @Override
                public void onError(String message) {
                    mResultView.onErrorCode();
                    mResultView.showInfo(message);
                }
            });
        }
    }

    @Override
    public void clearCache() {
        mhelper.deleteDatabase();
        OpenHelperManager.releaseHelper();
        mhelper = new DatabaseHelper(mContext);
        OpenHelperManager.setHelper(mhelper);
    }

    @Override
    public void setFranja(String franjaCode) {
        mResultView.onLoading();
        mService.getFranjaById(franjaCode, new IResultCallback<Franja>() {
            @Override
            public void onSuccess(final Franja response) {
                Boolean saveResult = mhelper.setFranja(response);
                mResultView.finishLoading();
                if (saveResult) {
                    mResultView.showInfo("La franja se ha configurado correctamente");
                    mResultView.scanCode();
                } else {
                    mResultView.showInfo("Ocurrio un error al configurar la franja");
                    return;
                }

                //new thread to download in background tickets
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        getFranjaData(response.getId());
                    }
                });
                t.start();
            }

            @Override
            public void onError(String message) {
                mResultView.showErrorMessage(message);

            }
        });
    }

    @Override
    public boolean isFranjaSetted() {
        Franja franja = mhelper.getFirstFranja();
        return franja != null ? true : false;
    }

    private void getFranjaData(String franjaId) {

        //TODO: Refactor code
        mService.getProductsByFranja(franjaId, new IResultCallback<ArrayList<Product>>() {
            @Override
            public void onSuccess(ArrayList<Product> response) {
                final Ticket ticket = new Ticket();
                for (int i = 0; i < response.size(); i++) {
                    final Product product = response.get(i);
                    mService.getFranjaById(product.getFrangaId(), new IResultCallback<Franja>() {
                        @Override
                        public void onSuccess(final Franja franja) {

                            mService.getOrderLinesByProductId(product.getId(), new IResultCallback<ArrayList<OrderLine>>() {
                                @Override
                                public void onSuccess(ArrayList<OrderLine> response) {
                                    for (int i = 0; i < response.size(); i++) {
                                        mService.getBoletosByOrderLine(response.get(i).getId(), new IResultCallback<ArrayList<Boleto>>() {
                                            @Override
                                            public void onSuccess(ArrayList<Boleto> response) {

                                                for (int i = 0; i < response.size(); i++) {
                                                    Ticket bdTicket = mhelper.getTicket(response.get(i).getValue());

                                                    if (bdTicket != null) continue;

                                                    ticket.setStartDate(franja.getFechaInicio());
                                                    ticket.setEndDate(franja.getFechaFin());
                                                    ticket.setId(response.get(i).getId());
                                                    ticket.setValue(response.get(i).getValue());
                                                    ticket.setIsUsed(false);
                                                    mhelper.addTicket(ticket);
                                                }
                                            }

                                            @Override
                                            public void onError(String message) {

                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onError(String message) {

                                }
                            });
                        }

                        @Override
                        public void onError(String message) {

                        }
                    });
                }
            }

            @Override
            public void onError(String message) {

            }
        });

    }


    private Boolean isTodayInBetween(String start, String end) {
        if (start == null || end == null) {
            Log.e(TAG, "isTodayInBetween: Start or end is null");
            return false;
        }

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            //TODO: take into account time zone.
            Date startDate = format.parse(start);
            Date endDate = format.parse(end);
            Date now = new Date();
            if (now.after(startDate) && now.before(endDate)) {
                return true;
            } else {
                Log.i(TAG, "La franja no incluye hoy.");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
