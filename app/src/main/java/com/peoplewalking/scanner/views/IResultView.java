package com.peoplewalking.scanner.views;

/**
 * Created by root on 02-23-16.
 */
public interface IResultView {
    void onSuccessCode();
    void onErrorCode();
    void onLoading();
    void showErrorMessage(String message);
    void showInfo(String message);
    void finishLoading();
    void scanCode();
}
